﻿using Microsoft.Xna.Framework;
using Nez;
using SpookyJam.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpookyJam.Scenes
{
    public class BaseScene : Scene
    {
        public override void initialize()
        {
            addRenderer(new DefaultRenderer());
            setDesignResolution(DimensionConstants.DESIGN_WIDTH, DimensionConstants.DESIGN_HEIGHT, SceneResolutionPolicy.ShowAllPixelPerfect);
            clearColor = Color.Black;
        }
    }
}
