﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using Nez;
using SpookyJam.Common;
using SpookyJam.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpookyJam.Scenes
{
    public class MainScene : BaseScene
    {
        public override void onStart()
        {
            var controller = createEntity("controller");
            controller.addComponent(new GameController(new List<MicroGame> { new SkeletonMatchGame(), new LichDogChase(), new GrimoireOpenGame() }));
        }
    }
}
