﻿using Microsoft.Xna.Framework;
using Nez;
using SpookyJam.Common;
using SpookyJam.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpookyJam.Scenes
{
    public class GameOverScene : Scene
    {
        public int Successes { get; set; }

        public GameOverScene(int successes)
        {
            Successes = successes;
        }

        public override void initialize()
        {
            addRenderer(new DefaultRenderer());
            setDesignResolution(DimensionConstants.DESIGN_WIDTH * 4, DimensionConstants.DESIGN_HEIGHT * 4, SceneResolutionPolicy.ShowAllPixelPerfect);
            clearColor = Color.Black;
        }

        public override void onStart()
        {
            var controller = createEntity("controller");
            controller.addComponent(new GameOverController(Successes));
        }
    }
}
