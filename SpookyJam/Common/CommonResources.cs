﻿using Microsoft.Xna.Framework.Graphics;
using Nez.BitmapFonts;
using Nez.TextureAtlases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpookyJam.Common
{
    public static class CommonResources
    {
        public static BitmapFont DefaultBitmapFont { get; set; }
        public static TextureAtlas SkeletonAtlas { get; set; }
        public static Texture2D Cursor { get; set; }
        public static Texture2D Pixel { get; set; }
        public static Texture2D LichDog { get; set; }
        public static Texture2D Necromancer { get; set; }
        public static Texture2D X { get; set; }
        public static Texture2D Circle { get; set; }
        public static Texture2D Grimoire { get; set; }
        public static TextureAtlas Symbols { get; set; }
        public static Texture2D Key { get; set; }
    }
}
