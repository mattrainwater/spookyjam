﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpookyJam.Common
{
    public static class DimensionConstants
    {
        public const int SCREEN_WIDTH = 1920;
        public const int SCREEN_HEIGHT = 1080;

        //public const int SCREEN_WIDTH = 1280;
        //public const int SCREEN_HEIGHT = 720;

        public const bool IS_FULL_SCREEN = false;

        public const int DESIGN_WIDTH = 80;
        public const int DESIGN_HEIGHT = 45;

        public const int CHARACTER_WIDTH = 64;
        public const int CHARACTER_HEIGHT = 64;

        public const int PORTRAIT_WIDTH = 256;
        public const int PORTRAIT_HEIGHT = 256;
    }
}
