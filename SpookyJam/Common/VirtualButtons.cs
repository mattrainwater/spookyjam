﻿using Microsoft.Xna.Framework.Input;
using Nez;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpookyJam.Common
{
    public static class VirtualButtons
    {
        public static VirtualButton SelectInput;
        public static VirtualButton BackInput;
        public static VirtualButton LeftInput;
        public static VirtualButton RightInput;
        public static VirtualButton DownInput;
        public static VirtualButton UpInput;

        public static VirtualIntegerAxis XAxisInput;
        public static VirtualIntegerAxis YAxisInput;

        public static void SetupInput()
        {
            SelectInput = new VirtualButton();
            SelectInput.nodes.Add(new Nez.VirtualButton.KeyboardKey(Keys.J));
            SelectInput.nodes.Add(new Nez.VirtualButton.GamePadButton(0, Buttons.A));

            BackInput = new VirtualButton();
            BackInput.nodes.Add(new Nez.VirtualButton.KeyboardKey(Keys.L));
            BackInput.nodes.Add(new Nez.VirtualButton.GamePadButton(0, Buttons.B));

            LeftInput = new VirtualButton();
            LeftInput.nodes.Add(new Nez.VirtualButton.KeyboardKey(Keys.A));
            LeftInput.nodes.Add(new Nez.VirtualButton.KeyboardKey(Keys.Left));
            LeftInput.nodes.Add(new Nez.VirtualButton.GamePadButton(0, Buttons.DPadLeft));
            LeftInput.nodes.Add(new Nez.VirtualButton.GamePadButton(0, Buttons.LeftThumbstickLeft));

            RightInput = new VirtualButton();
            RightInput.nodes.Add(new Nez.VirtualButton.KeyboardKey(Keys.D));
            RightInput.nodes.Add(new Nez.VirtualButton.KeyboardKey(Keys.Right));
            RightInput.nodes.Add(new Nez.VirtualButton.GamePadButton(0, Buttons.DPadRight));
            RightInput.nodes.Add(new Nez.VirtualButton.GamePadButton(0, Buttons.LeftThumbstickRight));

            DownInput = new VirtualButton();
            DownInput.nodes.Add(new Nez.VirtualButton.KeyboardKey(Keys.S));
            DownInput.nodes.Add(new Nez.VirtualButton.KeyboardKey(Keys.Down));
            DownInput.nodes.Add(new Nez.VirtualButton.GamePadButton(0, Buttons.DPadDown));
            DownInput.nodes.Add(new Nez.VirtualButton.GamePadButton(0, Buttons.LeftThumbstickDown));

            UpInput = new VirtualButton();
            UpInput.nodes.Add(new Nez.VirtualButton.KeyboardKey(Keys.W));
            UpInput.nodes.Add(new Nez.VirtualButton.KeyboardKey(Keys.Up));
            UpInput.nodes.Add(new Nez.VirtualButton.GamePadButton(0, Buttons.DPadUp));
            UpInput.nodes.Add(new Nez.VirtualButton.GamePadButton(0, Buttons.LeftThumbstickUp));

            XAxisInput = new VirtualIntegerAxis();
            XAxisInput.nodes.Add(new Nez.VirtualAxis.GamePadDpadLeftRight());
            XAxisInput.nodes.Add(new Nez.VirtualAxis.GamePadLeftStickX());
            XAxisInput.nodes.Add(new Nez.VirtualAxis.KeyboardKeys(VirtualInput.OverlapBehavior.TakeNewer, Keys.Left, Keys.Right));
            XAxisInput.nodes.Add(new Nez.VirtualAxis.KeyboardKeys(VirtualInput.OverlapBehavior.TakeNewer, Keys.A, Keys.D));

            YAxisInput = new VirtualIntegerAxis();
            YAxisInput.nodes.Add(new Nez.VirtualAxis.GamePadDpadUpDown());
            YAxisInput.nodes.Add(new Nez.VirtualAxis.GamePadLeftStickY());
            YAxisInput.nodes.Add(new Nez.VirtualAxis.KeyboardKeys(VirtualInput.OverlapBehavior.TakeNewer, Keys.Up, Keys.Down));
            YAxisInput.nodes.Add(new Nez.VirtualAxis.KeyboardKeys(VirtualInput.OverlapBehavior.TakeNewer, Keys.W, Keys.S));
        }
    }
}
