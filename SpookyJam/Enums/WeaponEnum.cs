﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpookyJam.Enums
{
    public enum WeaponEnum
    {
        Blades = 6,
        Fireball = 7,
        Curse = 8
    }
}
