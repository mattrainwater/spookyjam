﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpookyJam.Enums
{
    public enum SkeletonPartEnum
    {
        Head = 0,
        Body = 1,
        Weapon = 2
    }
}
