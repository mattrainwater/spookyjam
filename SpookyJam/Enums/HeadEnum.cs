﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpookyJam.Enums
{
    public enum HeadEnum
    {
        Aggressive = 0,
        Defensive = 1,
        Tricky = 2
    }
}
