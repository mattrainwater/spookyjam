﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Nez;
using Nez.TextureAtlases;
using Nez.Textures;
using SpookyJam.Common;
using SpookyJam.Scenes;
using System.Collections.Generic;

namespace SpookyJam
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Core
    {
        public Game1()
            : base(width: DimensionConstants.SCREEN_WIDTH, height: DimensionConstants.SCREEN_HEIGHT, isFullScreen: DimensionConstants.IS_FULL_SCREEN, windowTitle: "Arakara")
        {
            Core.defaultSamplerState = SamplerState.PointClamp;
        }

        protected override void Initialize()
        {
            base.Initialize();
            CommonResources.DefaultBitmapFont = Graphics.instance.bitmapFont;

            var skeletonTexture = contentManager.Load<Texture2D>("skeleton");
            var skeletonSubtextures = Subtexture.subtexturesFromAtlas(skeletonTexture, 24, 24);
            var skeletonTextureNames = new List<string>() {
                "aggresive_head", "defensive_head", "tricky_head",
                "steel_body", "fire_body", "bone_body",
                "blades_weapon", "fireball_weapon", "curse_weapon"
            };
            CommonResources.SkeletonAtlas = new TextureAtlas(skeletonTextureNames.ToArray(), skeletonSubtextures.ToArray());

            var symbolTexture = contentManager.Load<Texture2D>("symbols");
            var symbolSubtextures = Subtexture.subtexturesFromAtlas(symbolTexture, 4, 4);
            var symbolTextureNames = new List<string>() {
                "0", "1", "2",
                "3", "4", "5",
                "6", "7", "8"
            };
            CommonResources.Symbols = new TextureAtlas(symbolTextureNames.ToArray(), symbolSubtextures.ToArray());
            CommonResources.Cursor = contentManager.Load<Texture2D>("selector");
            CommonResources.Pixel = contentManager.Load<Texture2D>("pixel");
            CommonResources.Necromancer = contentManager.Load<Texture2D>("necromancer");
            CommonResources.LichDog = contentManager.Load<Texture2D>("lichdog");
            CommonResources.X = contentManager.Load<Texture2D>("x");
            CommonResources.Circle = contentManager.Load<Texture2D>("circle");
            CommonResources.Grimoire = contentManager.Load<Texture2D>("grimoire");
            CommonResources.Key = contentManager.Load<Texture2D>("key");
            scene = new TitleScene();
            VirtualButtons.SetupInput();
        }
    }
}
