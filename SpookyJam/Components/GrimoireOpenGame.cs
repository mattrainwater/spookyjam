﻿using Microsoft.Xna.Framework;
using Nez.Sprites;
using SpookyJam.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpookyJam.Components
{
    public class GrimoireOpenGame : MicroGame
    {
        public override string Name => "Open it!";

        private List<int> _selectSymbols;
        private int _currentSymbol;

        private List<Sprite> _locks;
        private Sprite _grimoire;
        private Sprite _key;
        private Sprite _currentSymbolSprite;

        public override void Initialize()
        {
            Success = false;
            Finished = false;

            _currentSymbol = 0;

            _selectSymbols = new List<int>();

            _grimoire = entity.addComponent(new Sprite(CommonResources.Grimoire));
            _grimoire.localOffset = new Vector2(40f, 8f);

            _key = entity.addComponent(new Sprite(CommonResources.Key));
            _key.localOffset = new Vector2(10f, 18f);

            _currentSymbolSprite = entity.addComponent(new Sprite(CommonResources.Symbols.subtextures[0]));
            _currentSymbolSprite.localOffset = new Vector2(12f, 20f);

            _locks = GenerateRandomLocks();
        }

        public override void Update()
        {
            // can cycle through keys like skeletons
            if(VirtualButtons.LeftInput.isPressed)
            {
                if(_currentSymbol == 0)
                {
                    _currentSymbol = 8;
                }
                else
                {
                    _currentSymbol--;
                }
                _currentSymbolSprite.subtexture = CommonResources.Symbols.subtextures[_currentSymbol];
            }
            else if(VirtualButtons.RightInput.isPressed)
            {
                if (_currentSymbol == 8)
                {
                    _currentSymbol = 0;
                }
                else
                {
                    _currentSymbol++;
                }
                _currentSymbolSprite.subtexture = CommonResources.Symbols.subtextures[_currentSymbol];
            }

            // on "enter" check if next symbol on book is correct
            if(VirtualButtons.SelectInput.isPressed)
            {
                if(_selectSymbols.First() == _currentSymbol)
                {
                    // if so remove it and if there are no more, success
                    _selectSymbols.Remove(_selectSymbols.First());
                    entity.removeComponent(_locks.First());
                    _locks.Remove(_locks.First());
                    if(!_selectSymbols.Any())
                    {
                        Finished = true;
                        Success = true;
                    }
                }
                else
                {
                    // else failure
                    Finished = true;
                    Success = false;
                }

            }
        }

        public override void Unload()
        {
            _selectSymbols = null;
            entity.removeAllComponents();
        }

        private List<Sprite> GenerateRandomLocks()
        {
            var sprites = new List<Sprite>();
            for(var i = 0; i < 3; i++)
            {
                int randomInt;
                do
                {
                    randomInt = Nez.Random.range(0, 9);
                }
                while (_selectSymbols.Contains(randomInt));
                _selectSymbols.Add(randomInt);
                var sprite = entity.addComponent(new Sprite(CommonResources.Symbols.subtextures[randomInt]));
                sprite.localOffset = new Vector2(47f + 5f * i, 20f);
                sprites.Add(sprite);
            }

            return sprites;
        }
    }
}
