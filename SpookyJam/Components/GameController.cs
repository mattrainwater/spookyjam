﻿using Microsoft.Xna.Framework;
using Nez;
using Nez.Sprites;
using SpookyJam.Common;
using SpookyJam.Scenes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpookyJam.Components
{
    public class GameController : Nez.Component, IUpdatable
    {
        private FillupBar _fillupBar;
        private float _timeElapsed = 0f;
        private float _totalTime = 30f;

        private float _titleDisplayElapsed = 0f;
        private float _titleDisplayTime = .5f;
        private bool _displayingTitle;
        private bool _firstPass;
        private Text _titleText;

        private int _successes = 0;

        private bool _transitioning;

        private Entity _currentGameEntity;
        private MicroGame _currentGame;

        public List<MicroGame> MicroGames { get; set; }

        private Sprite _circle;
        private Sprite _x;
        private bool _displayingSuccess;

        public GameController(List<MicroGame> games)
        {
            MicroGames = games;
        }

        public override void onAddedToEntity()
        {
            _fillupBar = entity.addComponent(new FillupBar(1, 60, Color.White, Color.DarkSlateGray));
            _fillupBar.setLocalOffset(new Vector2(10, 3));
            _currentGameEntity = entity.scene.createEntity("current_game");
            if (_currentGameEntity.scene == null)
            {
                _currentGameEntity.scene = entity.scene;
            }
            _titleText = entity.addComponent(new Text(CommonResources.DefaultBitmapFont, "", new Vector2(17f, 15f), Color.White));
            _titleText.renderLayer = -4;
            _titleText.enabled = false;

            _circle = entity.addComponent(new Sprite(CommonResources.Circle));
            _circle.enabled = false;
            _circle.renderLayer = -1;
            _circle.localOffset = new Vector2(23f, 7f);
            _x = entity.addComponent(new Sprite(CommonResources.X));
            _x.enabled = false;
            _x.renderLayer = -1;
            _x.localOffset = new Vector2(23f, 7f);
        }

        public void update()
        {
            if(!_displayingTitle && !_displayingSuccess && _firstPass)
            {
                _timeElapsed += Time.deltaTime;
                if (_timeElapsed > _totalTime)
                {
                    // game over
                    if (!_transitioning)
                    {
                        Core.startSceneTransition(new FadeTransition(LoadGameOverScene) {
                            fadeInDuration = 0f,
                            fadeOutDuration = 0f,
                            delayBeforeFadeInDuration = 0f,
                            fadeToColor = Color.WhiteSmoke });
                        _transitioning = true;
                    }
                }
            }
            if(_displayingSuccess)
            {
                _titleDisplayElapsed += Time.deltaTime;
                if (_titleDisplayElapsed > _titleDisplayTime)
                {
                    _titleDisplayElapsed = 0f;
                    _displayingSuccess = false;
                    _x.enabled = false;
                    _circle.enabled = false;
                    _displayingTitle = true;
                }
            }
            else if (_displayingTitle)
            {
                _firstPass = true;
                _titleDisplayElapsed += Time.deltaTime;
                _titleText.enabled = true;
                if (_titleDisplayElapsed > _titleDisplayTime)
                {
                    _titleDisplayElapsed = 0f;
                    _displayingTitle = false;
                    _titleText.setText("");
                    _titleText.enabled = false;
                    if (_currentGame.entity == null)
                    {
                        _currentGame.entity = _currentGameEntity;
                    }
                    _currentGame.Initialize();
                }
            }
            else
            {
                _fillupBar.CurrentWidth = (1 - _timeElapsed / _totalTime) * _fillupBar.MaxWidth;
                if (_currentGame == null || _currentGame.Finished)
                {
                    if (_currentGame != null && _currentGame.Finished && _currentGame.Success)
                    {
                        _circle.enabled = true;
                        _displayingSuccess = true;
                    }
                    else if(_currentGame != null && _currentGame.Finished && !_currentGame.Success)
                    {
                        _x.enabled = true;
                        _displayingSuccess = true;
                    }

                    if (_currentGame != null)
                    {
                        _currentGame.Unload();
                        _currentGameEntity.removeComponent(_currentGame);
                        _successes = _currentGame.Success ? _successes + 1 : _successes - 1;
                    }
                    else
                    {
                        _displayingTitle = true;
                    }
                    _currentGame = GetNextGame();
                    _currentGameEntity.addComponent(_currentGame);
                    _titleText.setText(_currentGame.Name);
                }
                if (!_displayingTitle && !_displayingSuccess)
                {
                    _currentGame.Update();
                }
            }
        }

        private MicroGame GetNextGame()
        {
            if(_currentGame == null)
            {
                return MicroGames.First();
            }
            var index = MicroGames.IndexOf(_currentGame);
            index++;
            if (index == MicroGames.Count())
            {
                index = 0;
            }
            return MicroGames[index];
        }

        private Scene LoadGameOverScene()
        {
            return new GameOverScene(_successes);
        }
    }
}
