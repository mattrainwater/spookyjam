﻿using Nez;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpookyJam.Components
{
    public abstract class MicroGame : Nez.Component
    {
        public bool Success { get; set; }

        public bool Finished { get; set; }

        public abstract string Name { get; }

        public abstract void Initialize();

        public abstract void Update();

        public abstract void Unload();
    }
}
