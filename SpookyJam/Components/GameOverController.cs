﻿using Microsoft.Xna.Framework;
using Nez;
using SpookyJam.Common;
using SpookyJam.Scenes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpookyJam.Components
{
    public class GameOverController : Nez.Component, IUpdatable
    {
        public int Successes { get; set; }

        private Dictionary<int, string> _titles = new Dictionary<int, string>
        {
            { 0, "Boneheaded Initiate" },
            { 1, "Pretty Bad Bonedude" },
            { 2, "Mediocre Bone Magician" },
            { 3, "Apprentice Boneman" },
            { 4, "Learned Bonebuilder" },
            { 5, "Average Bone-o-mancer" },
            { 6, "Great Bone Magician Guy" },
            { 7, "Excellent Extractor of Bones" },
            { 8, "Superb Skelemancer" },
            { 9, "Necrowhiz" },
            { 10, "Lord of the Undead, Dead, and Living Dead" },
        };

        private Text _correctText;
        private Text _youAreText;
        private Text _titleText;
        private Text _continueText;
        private bool _transitioning;

        private float _timeElapsed;

        public GameOverController(int successes)
        {
            Successes = successes;
        }

        public void update()
        {
            _timeElapsed += Time.deltaTime;
            if(_timeElapsed >= .5f && !_youAreText.enabled)
            {
                _youAreText.enabled = true;
            }
            if(_timeElapsed >= 1.5f && !_titleText.enabled)
            {
                _titleText.enabled = true;
            }
            if (_timeElapsed >= 2.5f && !_continueText.enabled)
            {
                _continueText.enabled = true;
            }
            if(_continueText.enabled && VirtualButtons.SelectInput.isPressed && !_transitioning)
            {
                _transitioning = true;
                Core.startSceneTransition(new FadeTransition(LoadTitleScene) { fadeInDuration = 0f, fadeOutDuration = 0f, delayBeforeFadeInDuration = 0f, fadeToColor = Color.Black });
            }
        }

        public override void onAddedToEntity()
        {
            _correctText = entity.addComponent(new Text(CommonResources.DefaultBitmapFont, $"You got {Successes} correct.", new Vector2(60f, 20f), Color.White));

            _youAreText = entity.addComponent(new Text(CommonResources.DefaultBitmapFont, "You are a...", new Vector2(60f, 50f), Color.White));
            _youAreText.enabled = false;

            _titleText = entity.addComponent(new Text(CommonResources.DefaultBitmapFont, GetTitle(), new Vector2(60f, 80f), Color.White));
            _titleText.enabled = false;

            _continueText = entity.addComponent(new Text(CommonResources.DefaultBitmapFont, "Try Again?", new Vector2(180f, 110f), Color.White));
            _continueText.enabled = false;
        }

        private string GetTitle()
        {
            if (_titles.ContainsKey(Successes))
            {
                return _titles[Successes];
            }
            if (Successes > 0)
            {
                return _titles.Values.Last();
            }
            else
            {
                return _titles.Values.First();
            }
        }

        private Scene LoadTitleScene()
        {
            return new TitleScene();
        }
    }
}
