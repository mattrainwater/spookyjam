﻿using System;
using Nez;
using Microsoft.Xna.Framework;
using SpookyJam.Common;
using Nez.Sprites;

namespace SpookyJam.Components
{
    public class LichDog : Component, IUpdatable
    {
        private Mover _mover;
        private float _moveSpeed = 75f;
        public Rectangle Bounds { get; set; }

        private Vector2? _currentTarget;
        private DogCatcher _catcher;

        public LichDog(DogCatcher catcher)
        {
            _catcher = catcher;
            _currentTarget = null;
        }

        public override void onAddedToEntity()
        {
            _mover = entity.addComponent(new Mover());
            Bounds = new Rectangle(0, 0, 10, 10);
            entity.addComponent(new Sprite(CommonResources.LichDog));
        }

        public void update()
        {
            if(_currentTarget == null)
            {
                _currentTarget = NewRandomTarget();
            }
            var moveDir = Vector2.Normalize(_currentTarget.Value - entity.transform.position);
            if (moveDir != Vector2.Zero && !moveDir.isNaN())
            {
                var movement = moveDir * _moveSpeed * Time.deltaTime;

                CollisionResult res;
                _mover.move(movement, out res);
                entity.transform.setPosition((int)entity.transform.position.X, (int)entity.transform.position.Y);
            }
            if(entity.transform.position == _currentTarget || moveDir.isNaN())
            {
                _currentTarget = NewRandomTarget();
            }
            Bounds = new Rectangle((int)entity.transform.position.X, (int)entity.transform.position.Y, 10, 10);
        }

        private Vector2 NewRandomTarget()
        {
            // if far away, dont move
            if(Vector2.Distance(entity.transform.position, _catcher.transform.position) > 35f && _currentTarget != null)
            {
                return _currentTarget.Value;
            }

            float newY = 0f;
            float newX = 0f;
            // if above catcher, go up
            if(_catcher.transform.position.Y <= transform.position.Y)
            {
                newY = Nez.Random.range(_catcher.transform.position.Y + 10, 31);
            }
            // if below catcher, go down
            else
            {
                newY = Nez.Random.range(5, _catcher.transform.position.Y - 10);
            }
            // if right of catcher, go right
            if (_catcher.transform.position.X <= transform.position.X)
            {
                newX = Nez.Random.range(_catcher.transform.position.X + 10, 66);
            }
            // if left of catch, go left
            else
            {
                newX = Nez.Random.range(5, _catcher.transform.position.X - 10);
            }

            return new Vector2((int)newX, (int)newY);
        }
    }
}