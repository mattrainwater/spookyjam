﻿using Microsoft.Xna.Framework;
using Nez;
using Nez.Sprites;
using SpookyJam.Common;
using SpookyJam.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpookyJam.Components
{
    public class SkeletonMatchGame : MicroGame
    {
        private Skeleton _skeleton;
        private Skeleton _enemySkeleton;

        public override string Name => "Match it!";

        private SkeletonPartEnum _selectedPart = SkeletonPartEnum.Head;
        private Sprite _leftCursor;
        private Sprite _rightCursor;
        private List<Vector2> _leftCursorPositions = new List<Vector2>
        {
            new Vector2(8, 10),
            new Vector2(2, 17),
            new Vector2(1, 23),
        };

        private List<Vector2> _rightCursorPositions = new List<Vector2>
        {
            new Vector2(27, 10),
            new Vector2(33, 17),
            new Vector2(34, 23),
        };

        public override void Initialize()
        {
            Success = false;
            Finished = false;
            _selectedPart = SkeletonPartEnum.Head;

            var skeleton = entity.scene.createEntity(Name);
            _skeleton = skeleton.addComponent(Skeleton.CreateRandomSkeleton());
            skeleton.transform.setPosition(10, 10);

            var enemySkeleton = entity.scene.createEntity(Name);
            _enemySkeleton = enemySkeleton.addComponent(Skeleton.CreateRandomSkeleton());
            enemySkeleton.transform.setPosition(50, 10);

            _leftCursor = entity.addComponent(new Sprite(CommonResources.Cursor));
            _leftCursor.setLocalOffset(_leftCursorPositions[0]);
            _rightCursor = entity.addComponent(new Sprite(CommonResources.Cursor));
            _rightCursor.flipX = true;
            _rightCursor.setLocalOffset(_rightCursorPositions[0]);
        }

        public override void Update()
        {
            if (VirtualButtons.DownInput.isPressed)
            {
                MoveCursor(false);
            }
            if (VirtualButtons.UpInput.isPressed)
            {
                MoveCursor(true);
            }
            if (VirtualButtons.LeftInput.isPressed)
            {
                ChangePart(_selectedPart, true);
            }
            if (VirtualButtons.RightInput.isPressed)
            {
                ChangePart(_selectedPart, false);
            }
            if(VirtualButtons.SelectInput.isPressed)
            {
                Success = MatchSkeltons();
                Finished = true;
            }
        }

        public override void Unload()
        {
            // remove all components
            entity.removeAllComponents();
            _skeleton.entity.destroy();
            _enemySkeleton.entity.destroy();
        }

        private bool MatchSkeltons()
        {
            return _skeleton.Head == _enemySkeleton.Head && _skeleton.Body == _enemySkeleton.Body
                && _skeleton.Weapon == _enemySkeleton.Weapon;
        }

        private void ChangePart(SkeletonPartEnum part, bool left)
        {
            switch(part)
            {
                case SkeletonPartEnum.Body:
                    var nextBody = left ? 
                        (int)_skeleton.Body - 1 == 2 ? BodyEnum.Bone : _skeleton.Body - 1 
                        : 
                        (int)_skeleton.Body + 1 == 6 ? BodyEnum.Steel : _skeleton.Body + 1;
                    _skeleton.ChangeBody(nextBody);
                    break;
                case SkeletonPartEnum.Head:
                    var nextHead = left ?
                        (int)_skeleton.Head - 1 == -1 ? HeadEnum.Tricky : _skeleton.Head - 1
                        :
                        (int)_skeleton.Head + 1 == 3 ? HeadEnum.Aggressive : _skeleton.Head + 1;
                    _skeleton.ChangeHead(nextHead);
                    break;
                case SkeletonPartEnum.Weapon:
                    var nextWeapon = left ?
                        (int)_skeleton.Weapon - 1 == 5 ? WeaponEnum.Curse : _skeleton.Weapon - 1
                        :
                        (int)_skeleton.Weapon + 1 == 9 ? WeaponEnum.Blades : _skeleton.Weapon + 1;
                    _skeleton.ChangeWeapon(nextWeapon);
                    break;
                default:
                    break;
            }
        }

        private void MoveCursor(bool up)
        {
            if(up)
            {
                var nextEnum = (int)_selectedPart - 1 == -1 ? SkeletonPartEnum.Weapon : _selectedPart - 1;
                _selectedPart = nextEnum;
            }
            else
            {
                var nextEnum = (int)_selectedPart + 1 == 3 ? SkeletonPartEnum.Head : _selectedPart + 1;
                _selectedPart = nextEnum;
            }
            _leftCursor.setLocalOffset(_leftCursorPositions[(int)_selectedPart]);
            _rightCursor.setLocalOffset(_rightCursorPositions[(int)_selectedPart]);
        }
    }
}
