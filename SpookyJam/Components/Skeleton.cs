﻿using Microsoft.Xna.Framework;
using Nez;
using Nez.Sprites;
using SpookyJam.Common;
using SpookyJam.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpookyJam.Components
{
    public class Skeleton : Nez.Component
    {
        public HeadEnum Head { get; set; }
        public BodyEnum Body { get; set; }
        public WeaponEnum Weapon { get; set; }

        private Sprite _headSprite;
        private Sprite _bodySprite;
        private Sprite _weaponSprite;

        public static Skeleton CreateRandomSkeleton()
        {
            var newHead = Nez.Random.range(0, 3);
            var newBody = Nez.Random.range(3, 6);
            var newWeapon = Nez.Random.range(6, 9);
            var skel = new Skeleton((HeadEnum)newHead, (BodyEnum)newBody, (WeaponEnum)newWeapon);
            return skel;
        }

        public Skeleton(HeadEnum head, BodyEnum body, WeaponEnum weapon)
        {
            Head = head;
            Body = body;
            Weapon = weapon;
        }

        public override void onAddedToEntity()
        {
            _headSprite = entity.addComponent(new Sprite(CommonResources.SkeletonAtlas.subtextures[(int)Head]));
            _headSprite.renderLayer = 2;

            _bodySprite = entity.addComponent(new Sprite(CommonResources.SkeletonAtlas.subtextures[(int)Body]));
            _bodySprite.renderLayer = 1;

            _weaponSprite = entity.addComponent(new Sprite(CommonResources.SkeletonAtlas.subtextures[(int)Weapon]));
            _weaponSprite.renderLayer = 0;
        }

        public void Randomize()
        {
            var newHead = Nez.Random.range(0, 3);
            var newBody = Nez.Random.range(3, 6);
            var newWeapon = Nez.Random.range(6, 9);
            ChangeHead((HeadEnum)newHead);
            ChangeBody((BodyEnum)newBody);
            ChangeWeapon((WeaponEnum)newWeapon);
        }

        public void ChangeHead(HeadEnum head)
        {
            entity.removeComponent(_headSprite);
            Head = head;
            _headSprite = entity.addComponent(new Sprite(CommonResources.SkeletonAtlas.subtextures[(int)Head]));
            _headSprite.renderLayer = 2;
        }

        public void ChangeBody(BodyEnum body)
        {
            entity.removeComponent(_bodySprite);
            Body = body;
            _bodySprite = entity.addComponent(new Sprite(CommonResources.SkeletonAtlas.subtextures[(int)Body]));
            _bodySprite.renderLayer = 1;
        }

        public void ChangeWeapon(WeaponEnum weapon)
        {
            entity.removeComponent(_weaponSprite);
            Weapon = weapon;
            _weaponSprite = entity.addComponent(new Sprite(CommonResources.SkeletonAtlas.subtextures[(int)Weapon]));
            _weaponSprite.renderLayer = 0;
        }
    }
}
