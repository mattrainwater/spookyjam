﻿using System;
using Nez;
using Microsoft.Xna.Framework;
using SpookyJam.Common;
using Nez.Sprites;

namespace SpookyJam.Components
{
    public class DogCatcher : Component, IUpdatable
    {
        private Mover _mover;
        private float _moveSpeed = 35f;

        public Rectangle Bounds { get; set; }

        public override void onAddedToEntity()
        {
            _mover = entity.addComponent(new Mover());
            Bounds = new Rectangle(0, 0, 10, 10);
            entity.addComponent(new Sprite(CommonResources.Necromancer));
        }

        public void update()
        {
            var moveDir = new Vector2( VirtualButtons.XAxisInput.value, VirtualButtons.YAxisInput.value );
            if (moveDir != Vector2.Zero)
            {
                var movement = moveDir * _moveSpeed * Time.deltaTime;

                CollisionResult res;
                _mover.move(movement, out res);
                entity.transform.setPosition((int)entity.transform.position.X, (int)entity.transform.position.Y);
            }
            Bounds = new Rectangle((int)entity.transform.position.X - 3, (int)entity.transform.position.Y - 3, 16, 16);
        }
    }
}