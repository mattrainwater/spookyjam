﻿using Nez;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using SpookyJam.Common;

namespace SpookyJam.Components
{
    public class LichDogChase : MicroGame
    {
        private LichDog _dog;
        private DogCatcher _catcher;

        public override string Name => "Catch him!";

        public override void Initialize()
        {
            Success = false;
            Finished = false;


            var catcher = entity.scene.createEntity(Name);
            _catcher = catcher.addComponent(new DogCatcher());
            _catcher.transform.setPosition(new Vector2(5, 30));

            var lichDog = entity.scene.createEntity(Name);
            _dog = lichDog.addComponent(new LichDog(_catcher));
            var dogPos = new Vector2(65, 5);
            _dog.transform.setPosition(dogPos);
        }

        public override void Unload()
        {
            // remove all components
            entity.removeAllComponents();
            _dog.entity.destroy();
            _catcher.entity.destroy();
        }

        public override void Update()
        {
            if(VirtualButtons.SelectInput.isPressed)
            {
                Finished = true;
                if (_catcher.Bounds != null && _dog.Bounds != null && _catcher.Bounds.Intersects(_dog.Bounds))
                {
                    Success = true;
                }
                else
                {
                    Success = false;
                }
            }
        }
    }
}
