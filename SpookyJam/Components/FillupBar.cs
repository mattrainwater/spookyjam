﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Nez;
using SpookyJam.Common;

namespace SpookyJam.Components
{
    public class FillupBar : RenderableComponent
    {
        public int Height { get; set; }
        public float CurrentWidth { get; set; }
        public int MaxWidth { get; set; }
        public Color BarColor { get; set; }
        public Color BackgroundColor { get; set; }

        public FillupBar(int height, int width, Color barColor, Color backgroundColor)
        {
            Height = height;
            CurrentWidth = width;
            MaxWidth = width;
            BarColor = barColor;
            BackgroundColor = backgroundColor;
        }

        public override RectangleF bounds
        {
            get
            {
                if (_areBoundsDirty)
                {
                    _bounds.calculateBounds(entity.transform.position, _localOffset, Vector2.Zero, entity.transform.scale, entity.transform.rotation, MaxWidth, Height);
                    _areBoundsDirty = false;
                }

                return _bounds;
            }
        }

        public override void render(Graphics graphics, Camera camera)
        {
            var backGroundScale = new Vector2(MaxWidth, Height);
            graphics.batcher.draw(CommonResources.Pixel, entity.transform.position + localOffset, null, BackgroundColor, entity.transform.rotation, Vector2.Zero, backGroundScale, SpriteEffects.None, _layerDepth);

            var barScale = new Vector2(CurrentWidth, Height);
            graphics.batcher.draw(CommonResources.Pixel, entity.transform.position + localOffset, null, BarColor, entity.transform.rotation, Vector2.Zero, barScale, SpriteEffects.None, _layerDepth);
        }
    }
}