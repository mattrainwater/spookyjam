﻿using Microsoft.Xna.Framework;
using Nez;
using SpookyJam.Common;
using SpookyJam.Scenes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpookyJam.Components
{
    public class TitleController : Component, IUpdatable
    {
        private bool _transitioning;
        private Text _titleText;

        public override void onAddedToEntity()
        {
            _titleText = entity.addComponent(new Text(CommonResources.DefaultBitmapFont, $"Necrio-Ware", new Vector2(40f, 60f), Color.White));
        }

        public void update()
        {
            if (VirtualButtons.SelectInput.isPressed && !_transitioning)
            {
                _transitioning = true;
                Core.startSceneTransition(new FadeTransition(LoadMainGameScene) { fadeInDuration = 0f, fadeOutDuration = 0f, delayBeforeFadeInDuration = 0f, fadeToColor = Color.Black });
            }
        }

        private Scene LoadMainGameScene()
        {
            return new MainScene();
        }
    }
}
